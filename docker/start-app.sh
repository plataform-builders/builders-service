#!/bin/bash

java -jar  -Dspring.profiles.active=prod -Dspring.datasource.url=$SPRING_DATASOURCE_URL  -Dspring.datasource.username=$SPRING_DATASOURCE_USERNAME -Dspring.datasource.platform=$SPRING_DATASOURCE_PLATFORM -Dspring.datasource.password=$SPRING_DATASOURCE_PASSWORD -Dspring.datasource.driver-class-name=$SPRING_DATASOURCE_DRIVER-CLASS-NAME sisget-service.jar
java -jar  -Dspring.profiles.active=prod -Dspring.datasource.username=root  -Dspring.datasource.password=root  -Dspring.datasource.url= jdbc:h2:mem:sisget-local;MODE=MYSQL builders-service.jar