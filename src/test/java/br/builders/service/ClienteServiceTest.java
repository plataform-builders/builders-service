package br.builders.service;

import br.builders.entities.Cliente;
import br.builders.exception.ClienteException;
import br.builders.repository.ClienteRepository;
import br.builders.service.cliente.FiltroClienteFactory;
import br.builders.service.dto.ClienteDTO;
import br.builders.service.dto.FiltroClienteDTO;
import br.builders.support.mapper.impl.ClienteMapper;
import br.builders.util.ClienteConstantes;
import br.builders.util.MockCliente;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.BIG_DECIMAL;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ClienteServiceTest {

    private ClienteService clienteService;
    @Mock
    private ClienteRepository clienteRepository;

    @Before
    public void setUp() throws Exception {
        clienteService = new ClienteService(clienteRepository, new ClienteMapper());
    }

    @Test
    public void salvar() {
        final ArgumentCaptor<Cliente> clienteCaptor = ArgumentCaptor.forClass(Cliente.class);
        final LocalDate dataNascimento = LocalDate.of(1990, 1, 1);

        final ClienteDTO clienteDTO = ClienteDTO.builder().nome("Teste").email("teste@gmail.com")
                .numeroRegistro("980.129.080-34").dataNascimento(dataNascimento).build();

        when(clienteRepository.findByEmail("teste@gmail.com")).thenReturn(Optional.empty());
        clienteService.salvar(clienteDTO);


        Mockito.verify(clienteRepository).save(clienteCaptor.capture());
        Cliente cliente = clienteCaptor.getValue();
        assertThat(cliente).isNotNull();
        assertThat(cliente.getNome()).isEqualTo("Teste");
        assertThat(cliente.getEmail()).isEqualTo("teste@gmail.com");
        assertThat(cliente.getNumeroRegistro()).isEqualTo("98012908034");
        assertThat(cliente.getDataNascimento()).isNotNull();
    }

    @Test
    public void retornarErroAoSalvarComEmailEncontrado() {
        final LocalDate dataNascimento = LocalDate.of(1990, 5, 1);

        final Cliente clienteBanco = MockCliente.getClienteBanco();

        final ClienteDTO clienteDTO = ClienteDTO.builder().nome("Teste 1234").email(clienteBanco.getEmail())
                .numeroRegistro("980.129.080-38").dataNascimento(dataNascimento).build();

        when(clienteRepository.findByEmail(clienteBanco.getEmail())).thenReturn(Optional.of(clienteBanco));
        ClienteException clienteException = assertThrows(ClienteException.class, () -> clienteService.salvar(clienteDTO));
        assertTrue(clienteException.getMenssagemErro().getCodigo().equals("MSG-016"));
        assertTrue(clienteException.getMenssagemErro().getMensagem().equals("Cliente já cadastrado para o e-mail "+clienteBanco.getEmail()));
    }


    @Test
    public void atualizarNomeENumeroRegistro() {
        final ArgumentCaptor<Cliente> clienteCaptor = ArgumentCaptor.forClass(Cliente.class);
        final ClienteDTO clienteParaAtualizacao = MockCliente.getClienteParaAtualizacao();
        final Cliente clienteBanco = MockCliente.getClienteBanco();


        when(clienteRepository.findByEmail("teste@gmail.com")).thenReturn(Optional.of(clienteBanco));
        clienteService.atualizar(clienteParaAtualizacao);

        Mockito.verify(clienteRepository).save(clienteCaptor.capture());
        Cliente clienteAtualizado = clienteCaptor.getValue();
        assertThat(clienteAtualizado).isNotNull();
        assertThat(clienteAtualizado.getId()).isEqualTo(1);
        assertThat(clienteAtualizado.getNome()).isEqualTo(clienteParaAtualizacao.getNome());
        assertThat(clienteAtualizado.getNumeroRegistro()).isEqualTo("98012908034");
    }

    @Test
    public void retornarErroAoAtualizarComClienteNaoEncontrado() {
        final LocalDate dataNascimento = LocalDate.of(1990, 5, 1);
        final ClienteDTO clienteDTO = ClienteDTO.builder().nome("Teste 1234").email("teste@teste.com.br")
                .numeroRegistro("980.129.080-38").dataNascimento(dataNascimento).build();

        when(clienteRepository.findByEmail(clienteDTO.getEmail())).thenReturn(Optional.empty());
        ClienteException clienteException = assertThrows(ClienteException.class, () -> clienteService.atualizar(clienteDTO));
        assertTrue(clienteException.getMenssagemErro().getCodigo().equals("MSG-015"));
        assertTrue(clienteException.getMenssagemErro().getMensagem().equals("Cliente não encontrado."));
    }

    @Test
    public void buscarClientePorNome() {
        final Cliente clienteBanco = MockCliente.getClienteBanco();

        when(clienteRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(
               new PageImpl(Arrays.asList(clienteBanco))
        );

        Page<ClienteDTO> clientes = clienteService.getClientesPorFiltro(
                FiltroClienteDTO.builder()
                                .nome("Teste")
                                .pageable(PageRequest.of(0, 10))
                                .build()
        );
        assertThat(clientes).isNotNull();
        assertThat(clientes.stream().findFirst().get().getNome()).isEqualTo("Teste");
    }

    @Test
    public void buscarClientePorNomeEEmail() {

        final Cliente clienteBanco = MockCliente.getClienteBanco();

        when(clienteRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(
                new PageImpl(Arrays.asList(clienteBanco))
        );

        final Page<ClienteDTO> clientes = clienteService.getClientesPorFiltro(
                FiltroClienteDTO.builder()
                        .nome("Teste")
                        .email("teste@gmail.com")
                        .build()
        );
        assertThat(clientes).isNotNull();
        assertThat(clientes.stream().findFirst().get().getNome()).isEqualTo("Teste");
        assertThat(clientes.stream().findFirst().get().getEmail()).isEqualTo("teste@gmail.com");
    }

    @Test
    public void buscarClientePorNumeroRegistro() {
        final Cliente clienteBanco = MockCliente.getClienteBanco();

        when(clienteRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(
                new PageImpl(Arrays.asList(clienteBanco))
        );

        final Page<ClienteDTO> clientes = clienteService.getClientesPorFiltro(
                FiltroClienteDTO.builder()
                        .numeroRegistro("98012908038")
                        .build()
        );
        assertThat(clientes).isNotNull();
        assertThat(clientes.stream().findFirst().get().getNumeroRegistro()).isEqualTo("98012908038");
    }
}