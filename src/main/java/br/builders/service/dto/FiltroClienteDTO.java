package br.builders.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.domain.PageRequest;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FiltroClienteDTO {

    private static final long serialVersionUID = -4113145618212549062L;
    public static final int NUMERO_PAGINA_PADRAO = 0;
    public static final int TAMANHAO_PAGINA_PADRAO = 10;

    @NotNull
    private PageRequest pageable;
    private String nome;
    private String email;
    private String telefone;
    private String numeroRegistro;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dataNascimento;

    public PageRequest getPageable() {
        if (Objects.isNull(pageable)) {
            return PageRequest.of(NUMERO_PAGINA_PADRAO, TAMANHAO_PAGINA_PADRAO);
        }

        return this.pageable;
    }
}
