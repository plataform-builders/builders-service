package br.builders.service;

import br.builders.entities.Cliente;
import br.builders.exception.ClienteException;
import br.builders.exception.MessageCode;
import br.builders.repository.ClienteRepository;
import br.builders.repository.specification.ClienteSpecification;
import br.builders.service.dto.ClienteDTO;
import br.builders.service.dto.FiltroClienteDTO;
import br.builders.support.mapper.impl.ClienteMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.springframework.data.jpa.domain.Specification.where;


/**
 * Responsável pela execução das operações do cadastro de cliente, onde será possível:
 * - Criação de novos clientes;
 * - Atualização de clientes existentes;
 * - Busca de informaçoes de clientes;
 */
@Slf4j
@RequiredArgsConstructor
@Service
@Transactional(rollbackFor = ClienteException.class)
public class ClienteService {

    private final ClienteRepository clienteRepository;
    private final ClienteMapper builder;

    public void salvar(final ClienteDTO clienteDTO) {
        verificarClienteCadastrado(clienteDTO);
        Cliente cliente = builder.converterDTOEmEntidade(clienteDTO);
        cliente.normalizarNumeroRegistro();
        clienteRepository.save(cliente);
    }

    private void verificarClienteCadastrado(final ClienteDTO clienteDTO) {
        Optional<Cliente> clienteOptional = clienteRepository.findByEmail(clienteDTO.getEmail());
        if (clienteOptional.isPresent()) {
            throw new ClienteException(MessageCode.CLIENTE_JA_CADASTRADO_EMAIL_INFORMADO, clienteDTO.getEmail());
        }
    }

    public void atualizar(final ClienteDTO clienteDTO) {
        clienteRepository.findByEmail(clienteDTO.getEmail())
                .ifPresentOrElse(cliente -> {
                    cliente.clearTelefones();
                    Cliente clienteEditado  = builder.converterDTOEmEntidade(clienteDTO);
                    BeanUtils.copyProperties(clienteEditado , cliente, "id");
                    cliente.normalizarNumeroRegistro();
                    clienteRepository.save(cliente);
             }, () -> {
                throw new ClienteException(MessageCode.CLIENTE_NAO_ENCONTRADO);
            }
        );
    }


    public Page<ClienteDTO> getClientesPorFiltro(final FiltroClienteDTO filtro) {
        Specification<Cliente> specification = getFiltroSpecification(filtro);
        Page<Cliente> clientes = clienteRepository.findAll(specification, filtro.getPageable());
        return clientes.map(cliente -> builder.converterEntidadeEmDTO(cliente));
    }


    private Specification<Cliente> getFiltroSpecification(FiltroClienteDTO filtro) {
        return where(ClienteSpecification.porNome(filtro.getNome()))
                                        .and(ClienteSpecification.porEmail(filtro.getEmail()))
                                        .and(ClienteSpecification.porTelefone(filtro.getTelefone()))
                                        .and(ClienteSpecification.porNumeroRegistro(filtro.getNumeroRegistro()))
                                        .and(ClienteSpecification.porDataNascimento(filtro.getDataNascimento()));
    }
}