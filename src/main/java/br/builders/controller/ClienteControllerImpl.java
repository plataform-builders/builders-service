package br.builders.controller;

import br.builders.controller.api.ClienteController;
import br.builders.service.ClienteService;
import br.builders.service.dto.ClienteDTO;
import br.builders.service.dto.FiltroClienteDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/clientes")
public class ClienteControllerImpl implements ClienteController {

    private final ClienteService clienteService;

    @Override
    public ResponseEntity<Page<ClienteDTO>> getClientesPorFiltro(final FiltroClienteDTO filtro) {
        Page<ClienteDTO> lista = clienteService.getClientesPorFiltro(filtro);
        return ResponseEntity.ok(lista);
    }

    @Override
    public ResponseEntity<Void> salvar(@RequestBody @Valid  final ClienteDTO clienteDTO) {
        clienteService.salvar(clienteDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Void> alterar(@RequestBody @Valid  final ClienteDTO clienteDTO) {
        clienteService.atualizar(clienteDTO);
        return ResponseEntity.ok().build();
    }
}
