package br.builders.controller.api;

import br.builders.service.dto.ClienteDTO;
import br.builders.service.dto.FiltroClienteDTO;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public interface ClienteController {


    @GetMapping
    @Operation(summary = "Permita que seja possível listar os clientes de forma paginada.")
    ResponseEntity<Page<ClienteDTO>> getClientesPorFiltro(@Valid final FiltroClienteDTO filtro);

    @PostMapping
    @Operation( summary =  "Permita criação de novos clientes. Obs: Formato da data de nascimento dd/MM/yyyy e o telefone apenas números")
    ResponseEntity<Void> salvar(@RequestBody final ClienteDTO clienteDTO);

    @Operation( summary = "Permita a atualização de clientes existentes. Obs: Formado da data de nascimento dd/MM/yyyy e o telefone apenas números")
    @PutMapping
    ResponseEntity<Void> alterar(@RequestBody final ClienteDTO clienteDTO);
}
